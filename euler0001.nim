# https://projecteuler.net/problem=1

# TODO optimize using triangular number formula

const bound = 1000
const factor_A = 3
const factor_B = 5
const factor_common = factor_A * factor_B

func sumMultiples*(m: int): int =
  result = 0
  var idx = 1
  var next_multiple = idx * m
  while next_multiple < bound:
    result += next_multiple
    idx += 1
    next_multiple = idx * m

var sum_A = sumMultiples(factor_A)
var sum_B = sumMultiples(factor_B)
var sum_common = sumMultiples(factor_common)

let total = sum_A + sum_B - sum_common

when isMainModule:
  echo total
