# https://projecteuler.net/problem=2

# Problem statement: Find the sum of all the even-valued Fibonacci numbers less
# than four million.

const bound = 4_000_000

# type
#   PositiveInt = range[1..high(int)]

# Memoize the sequence
var fibonaccis: seq[int]
fibonaccis = @[1, 1, 2, 3, 5, 8, 13, 21, 34, 55]

proc fib*(n: int): int =
  doAssert(n >= 0)
  if n <= high(fibonaccis):
    return fibonaccis[n]
  else:
    result = fib(n - 1) + fib(n - 2)
    fibonaccis.add(result)

proc memoTest() =
  echo "fibonaccis memoization test follows"
  echo fibonaccis
  echo fib(20)
  echo fibonaccis
  echo fib(19)

# Every third number in the sequence is even
proc solve(): int =
  result = 0
  var i = 2
  while fib(i) < bound:
    result += fib(i)
    i += 3

when isMainModule:
  echo solve()
